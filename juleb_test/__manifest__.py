{
    "name": "Juleb Test - Translation Module",
    "version": "13.0.1.0.0",
    "category": "Localization",
    "summary": "Juleb Test - Translation Module",
    "author": "Terry",
    "website": "https://terry.com",
    "license": "LGPL-3",
    "depends": ["base"],
    "installable": True,
    "application": False,
    "auto_install": True,
    "sequence": 101
}
